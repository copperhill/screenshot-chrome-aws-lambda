'use strict';

const chromium = require('chrome-aws-lambda');

module.exports.screenshot = async event => {

  const { urlBase64, viewWidth, viewHeight, deleteCookieName, deleteCookieDomain, deleteCookiePath } = event.queryStringParameters;
  let browser = null;

  try {
    browser = await chromium.puppeteer.launch({
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath: await chromium.executablePath,
      headless: chromium.headless,
    });
 
    let page = await browser.newPage();

    await page.setViewport({ width: parseInt(viewWidth, 10) || 1920, height: parseInt(viewHeight, 10) || 1080, isLandscape: true});
    var url = Buffer.from(urlBase64, 'base64').toString();
    await page.goto(url || 'https://google.com', { waitUntil: 'networkidle0' });
    if (deleteCookieName && deleteCookieName != '' && deleteCookiePath && deleteCookiePath != '' && deleteCookieDomain && deleteCookieDomain !='') {
      await page.deleteCookie({ name: deleteCookieName, domain: deleteCookieDomain, path: deleteCookiePath });
    }
    var screenshotBuffer = await page.screenshot({ fullPage: true });
 
  } catch (error) {
    return {
      statusCode: 500,
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      },
      body: JSON.stringify({ result: { success: false, msg: "Error: "+JSON.stringify(error) } })
    };
  } finally {
    if (browser !== null) {
      await browser.close();
    }
  }

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'image/png'
    },
    body: screenshotBuffer.toString('base64'),
    isBase64Encoded: true
  };
};
