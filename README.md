# Screenshot on Chromium

## Setup

- Create AWS IAM role with permissions for Lambda execution and CloudWatchFullAccess named 'screenshot-chrome-aws-lambda'

## Deployment

```npm i -g serverless```

```aws configure --profile {name}```

```sls deploy --aws-profile {profile name} --stage {dev|staging|prod}```

## Usage

The URL will be shown on ```sls deploy```. Use it with the below parameters.

### GET Parameters

- urlBase64: URL that is Base64 encoded
- viewWidth: width of the browser screen in pixels
- viewHeight: height of the browser screen in pixels

*(use together)*

- deleteCookieName: delete a cookie matching this name
- deleteCookieDomain: delete a cookie matching this domain
- deleteCookiePath: delete a cookie matching this path